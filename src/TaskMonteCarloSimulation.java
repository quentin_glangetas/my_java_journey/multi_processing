import java.util.concurrent.Callable;

public class TaskMonteCarloSimulation implements Callable<Integer> {

    final private int nSamples;

    public TaskMonteCarloSimulation(int nSamples) {
        this.nSamples = nSamples;
    }

    @Override
    public Integer call() {
        PiMonteCarloSimulation piMonteCarlo = new PiMonteCarloSimulation();
        int piSubEstimate = piMonteCarlo.evaluateNumberInsideCircle(nSamples);
        return piSubEstimate;
    }
}
