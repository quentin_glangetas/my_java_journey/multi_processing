import java.util.concurrent.ThreadLocalRandom;

public class PiMonteCarloSimulation {

    private ThreadLocalRandom rng;

    public PiMonteCarloSimulation() {
        rng = ThreadLocalRandom.current();
    }

    private boolean isValidSample(double x, double y) {
        return (x >= -1 - 1e-8) && (x <= 1 + 1e-8) && (y >= -1 - 1e-8) && (y <= 1 + 1e-8);
    }

    private boolean isInsideUnitCircle(double x, double y) {
        if (!isValidSample(x, y)) {
            return false;
        }
        return x * x + y * y <= 1;
    }

    private double sampleBetweenMinusOneAndOne() {
        return -1 + 2 * rng.nextDouble(); // rng.nextDouble() samples in [0, 1]. Scaling to sample for -1 to 1
    }

    public int evaluateNumberInsideCircle(int nSamples) {
        int nSampleInsideUnitCricle = 0;
        double x, y;
        for (int k = 0; k < nSamples; k++) {
            x = sampleBetweenMinusOneAndOne();
            y = sampleBetweenMinusOneAndOne();
            if (isInsideUnitCircle(x, y)) {
                nSampleInsideUnitCricle++;
            }
        }
        return nSampleInsideUnitCricle;
    }

    public double evaluatePi(int nSamples) {
        // Probability of a point (x, y) in [-1, 1] to be in unit circle is pi / 4
        return 4 * ((double) evaluateNumberInsideCircle(nSamples)) / nSamples;
    }
}
