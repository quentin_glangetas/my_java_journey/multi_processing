import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Main {
    public static void main(String[] args) throws Exception {
        int nIter = 100;
        int[] totalSampleGrid = new int[] { 100_000, 1_000_000, 10_000_000, 100_000_000 };
        int[] nThreadGrid = new int[] { 1, 2, 3, 4, 5, 6, 7, 8 };
        long start;
        long ellapsed;
        long ellapsedSum;
        long ellapsedSumSquared;
        double ellapsedMean;
        double ellapsedStd;

        FileWriter resultWriter = new FileWriter("results.csv");

        resultWriter.write("samples,threads,timeIter,avgEllapsed,stdEllapsed\n");
        for (int totalSamples : totalSampleGrid) {
            for (int nThreads : nThreadGrid) {
                System.out.printf("totalSamples = %,d | nThreads = %d\n", totalSamples, nThreads);
                ellapsedSum = 0;
                ellapsedSumSquared = 0;
                for (int k = 0; k < nIter; k++) {
                    start = System.currentTimeMillis();
                    estimatePi(totalSamples, nThreads);
                    ellapsed = System.currentTimeMillis() - start;
                    ellapsedSum += ellapsed;
                    ellapsedSumSquared += ellapsed * ellapsed;
                }
                ellapsedMean = ((double) ellapsedSum) / nIter;
                ellapsedStd = Math.sqrt(((double) ellapsedSumSquared) / nIter - ellapsedMean * ellapsedMean);
                resultWriter.write(String.format(
                        "%d,%d,%d,%f,%f\n", totalSamples, nThreads, nIter, ellapsedMean, ellapsedStd));
            }
        }

        resultWriter.close();
        System.out.println("Finished!");
    }

    private static double estimatePi(int totalSamples, int nThreads) {
        int subSamples = totalSamples / nThreads;
        ExecutorService pool = Executors.newFixedThreadPool(nThreads);
        List<Future<Integer>> simulationResults = new ArrayList<>(nThreads);

        for (int k = 0; k < nThreads; k++) {
            Future<Integer> threadEstimate = pool.submit(new TaskMonteCarloSimulation(subSamples));
            simulationResults.add(threadEstimate);
        }

        int totalSamplesInsideUnitCircle = simulationResults.stream().mapToInt(Main::processFutureThreadEstimate).sum();

        return 4 * ((double) totalSamplesInsideUnitCircle) / totalSamples;
    }

    private static int processFutureThreadEstimate(Future<Integer> futureInt) {
        try {
            return futureInt.get();
        } catch (InterruptedException | ExecutionException e) {
            System.out.println("Failed to run a thread. Please stop simulation.");
            return 0;
        }
    }
}
