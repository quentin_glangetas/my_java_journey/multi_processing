# MultiProcessing

This project enabled myself to learn about computing in parallel in Java, using the high level interface `Executors`. 

The context is simple. I used the excuse of a Monte Carlo Simulation, which estimates $\pi$, to use a multi processing paradigm to compute the simulation faster. I then consolidated the results by time, to have a visualization of the time gained by computing in parallel. 

# Results

Here are the ellapsed time results of the different simulations:
* Simulation samples varied from 100k to 100m points
* Number of processed (threads) varied from 1 to 8
* The error bars come from repeating each simulation 100 times

![Time comparison plot](ellapsed_time_comparison.png)

Observations:
* For smaller samples size (100k and 1m), there is a point where the overhead of launching more JMM doesn't improve performance, and actually worsen overall speed of execution
* The gains are not linear, and are more logarithmic in nature: akin to the law of diminishing returns


# A word on the simulation

The simulation is quite straightforward:

* The surface of a square of size 2x2 is $S_{square} = 4$. 
* The inscribed circle $\mathcal{C}$ of that square is of radius 1. Thus, of surface $S_{\mathcal{C}} = \pi * r^{2} = \pi$.
* A uniformly sampled point on the square has therefore a probability of $\cfrac{\pi}{4}$ of falling within the inscribed circle.
* We can estimate this probability using a Monte Carlo Simulation:
  * Select $N$ uniformly sampled points inside the 2x2 square: $P(x, y) \sim \mathcal{U}(-1, 1) \times \mathcal{U}(-1, 1)$
  * Then $ \hat{\pi} = \cfrac{4}{N} \sum_{i=1}^{N} 1_{P_i\in\mathcal{C}}$
  * $P_i\in\mathcal{C} \Longleftrightarrow x_i^{2} + y_i^{2} \le 1$

